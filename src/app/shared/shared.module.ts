import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HeaderComponent} from './header/header.component';
import {LeftSidebarComponent} from './left-sidebar/left-sidebar.component';
import {RightSidebarComponent} from './right-sidebar/right-sidebar.component';
import {RouterModule} from '@angular/router';

@NgModule({
  exports: [
    HeaderComponent,
    RightSidebarComponent,
    LeftSidebarComponent
  ],
  imports: [
    RouterModule
  ],
  declarations: [
    HeaderComponent,
    LeftSidebarComponent,
    RightSidebarComponent
  ]
})

export class SharedModule {
}
