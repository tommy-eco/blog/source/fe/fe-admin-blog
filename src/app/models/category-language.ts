export class CategoryLanguage {
  id: string;
  name: string;
  description: string;
  langCode: string;
}
