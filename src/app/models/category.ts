import {CategoryLanguage} from './category-language';

export class Category {
  id: string;
  categoryLanguages: CategoryLanguage[];
}
