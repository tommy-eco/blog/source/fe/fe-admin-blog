import { Component, OnInit } from '@angular/core';
import {CategoryService} from '../../../services/category.service';
import {Category} from '../../../models/category';
import {CategoryViewModel} from '../../../viewmodels/category-view-model';

@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.scss']
})
export class CategoryListComponent implements OnInit {
  categories: CategoryViewModel[];
  constructor(private categoryService: CategoryService) { }

  ngOnInit(): void {
    this.getAllCategory();
  }

  getAllCategory(): void {
    const langCode = 'vi-vn';
    this.categoryService.getAll(langCode).subscribe(data => {
      this.categories = data;
    });
  }
}
