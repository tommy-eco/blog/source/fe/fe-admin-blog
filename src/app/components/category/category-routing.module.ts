import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CategoryComponent} from './category.component';
import {CategoryListComponent} from './category-list/category-list.component';
import {CategoryCreateComponent} from './category-create/category-create.component';

const routes: Routes = [
  {path: '', component: CategoryComponent, children: [
      {path: '', redirectTo: 'lists', pathMatch: 'full'},
      {path: 'lists', component: CategoryListComponent},
      {path: 'create', component: CategoryCreateComponent}
    ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class CategoryRoutingModule { }
