import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {CategoryDTO} from '../../../dto/category-dto';
import {CategoryLanguageDTO} from '../../../dto/category-language-dto';
import {CategoryService} from '../../../services/category.service';

@Component({
  selector: 'app-category-create',
  templateUrl: './category-create.component.html',
  styleUrls: ['./category-create.component.scss']
})
export class CategoryCreateComponent implements OnInit {
  categoryForm: FormGroup;
  newCategory: CategoryDTO = new CategoryDTO();
  constructor(private categoryService: CategoryService) { }

  ngOnInit(): void {
    this.initForm();
    this.newCategory.categoryLanguages = [];
  }

  initForm(): void {
    this.categoryForm = new FormGroup({
      name: new FormControl(),
      description: new FormControl(),
      langCode: new FormControl('vi-vn')
    });
  }

  onSubmit(): void {
    let newCategoryLanguage = new CategoryLanguageDTO();
    newCategoryLanguage = Object.assign(newCategoryLanguage, this.categoryForm.value);
    this.newCategory.categoryLanguages.push(newCategoryLanguage);

    // call category service to create category
    this.createCategory(this.newCategory);
    this.categoryForm.reset();
  }

  createCategory(category: CategoryDTO): void {
    this.categoryService.create(category).subscribe(data => {
        if (data === 201){
          console.log('Created Successfully');
        }
    }, error => {
      console.log(error);
    });
  }
}
