import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Category} from '../models/category';
import {environment} from '../../environments/environment';
import {CategoryDTO} from '../dto/category-dto';
import {CategoryViewModel} from '../viewmodels/category-view-model';

@Injectable()
export class CategoryService {
  baseUrl = environment.BASE_URL;
  apiUrl = environment.API_URL.CATEGORY;

  constructor(private http: HttpClient) { }

  getAll(langCode: string): Observable<CategoryViewModel[]> {
    return this.http.get<CategoryViewModel[]>(`${this.baseUrl}/${this.apiUrl}?langCode=${langCode}`);
  }

  create(category: CategoryDTO): Observable<number> {
    return this.http.post<number>(`${this.baseUrl}/${this.apiUrl}`, category);
  }
}
