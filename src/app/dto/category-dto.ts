import {CategoryLanguageDTO} from './category-language-dto';

export class CategoryDTO {
  public categoryLanguages: CategoryLanguageDTO[];
}
