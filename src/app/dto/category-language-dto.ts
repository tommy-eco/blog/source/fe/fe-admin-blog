export class CategoryLanguageDTO {
  public name: string;
  public description: string;
  public langCode: string;
}
